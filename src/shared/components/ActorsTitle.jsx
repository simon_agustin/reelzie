import PropTypes from "prop-types";

import React from "react";

function ActorsTitle({headingTitle,textColor,transform,decoration}) {
  const titleStytles = {
    color: textColor,
    textTransform: transform,
    textDecoration: decoration,
  };

  return (
      <div className="actors-title" style={titleStytles}>
        <h1>{headingTitle}</h1>
      </div>
  );
}

ActorsTitle.defaultProps = {
  headingTitle: "actors",
  textColor: "#16a8d9",
  transform: "uppercase",
  decoration: "none",
};

ActorsTitle.propTypes = {
  headingitle: PropTypes.string,
  textColor: PropTypes.string,
  transform: PropTypes.string,
  decoration: PropTypes.string,
};

export default ActorsTitle;
