import PropTypes from "prop-types";
import React from "react";
function SignUpHeader({headingTitle, headingSubtitle, textColor,transform,decoration}) {
  const titleStytles = {
    color: textColor,
    textTransform: transform,
    textDecoration: decoration,
  };

  return (
      <div className="sign-up" style={titleStytles}>
        <h1>{headingTitle}</h1>
        <h2>choosing your pricing plan</h2>
      </div>
  );
}

SignUpHeader.defaultProps = {
  headingTitle: "sign up today",
  // headingSubtitle: "choosing your pricing plan",
  textColor: "#16a8d9",
  transform: "uppercase",
  decoration: "none",
};

SignUpHeader.propTypes = {
  headingitle: PropTypes.string,
  // headingSubtitle: PropTypes.string,
  textColor: PropTypes.string,
  transform: PropTypes.string,
  decoration: PropTypes.string,
};

export default SignUpHeader;
