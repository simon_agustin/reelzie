import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import React from "react";
function CallToAction({ bgColor, textColor, transform, decoration }) {
  const btnStyles = {
    backgroundColor: bgColor,
    color: textColor,
    textTransform: transform,
    textDecoration: decoration,
  };

  return (
    <Link to='/signup'>
    <div className="cta">{<button style={btnStyles}>sign up</button>}</div>
    </Link>
  );
}

CallToAction.defaultProps = {
  header1: "reelzie",
  bgColor: "transparent",
  textColor: "#16a8d9",
  transform: "uppercase",
  decoration: "none",
};

CallToAction.propTypes = {
  header1: PropTypes.string,
  bgColor: PropTypes.string,
  textColor: PropTypes.string,
  transform: PropTypes.string,
  decoration: PropTypes.string,
};

export default CallToAction;
