import { Link } from "react-router-dom";
import Categories from "../../pages/Categories";
// import Categories from "../../pages/Categories";
// import Home from "../../pages/Home";
// import { useLocation } from "react-router-dom";
// import { useState } from "react";
// import { useEffect } from "react";
// import { useNavigate } from "react-router-dom";

function NavMenu() {
  
  return (
    <div>
      <div className="nav-menu">
        <ul>
          <Link to="/home">
            <li>Home</li>
          </Link>
          <Link to="/categories">
            <li>Categories</li>
          </Link>
          <Link to="/shorts">
            <li>Shorts</li>
          </Link>
          <Link to="/joblistings">
            <li>Job Listings</li>
          </Link>
          <Link to="/colleges">
            <li>College Connection</li>
          </Link>
          <Link to="/boards">
            <li>Message Boards</li>
          </Link>
          <Link to="/privacy">
            <li>Privacy Policy</li>
          </Link>
        </ul>
      </div>
    </div>
  );
}

export default NavMenu;
