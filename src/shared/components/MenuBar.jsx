function MenuBar() {
  return (
    <div className="menu-bar">
      <div className="menu-bar__icon"></div>
    </div>
  );
}

window.onload = function () {
  const menuBar = document.querySelector(".menu-bar");
  const menuList = document.querySelector(".menu-list");

  let menuOpen = false;
  let listOpen = false;

  menuBar.addEventListener("click", () => {
    if (!menuOpen && !listOpen) {
      menuBar.classList.add("open");
      menuOpen = true;
      menuList.classList.add("open");
      listOpen = true;
      console.log(menuList)
    } else {
      menuBar.classList.remove("open");
      menuOpen = false;
      menuList.classList.remove("open");
      listOpen = false;
      console.log(menuList)
    }
  });
};

export default MenuBar;
