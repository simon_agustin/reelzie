import PropTypes from "prop-types";
import React from "react";

function VideoChannel({subHeadingTitle,textColor,decoration}) {
  const subtitleStytles = {
    color: textColor,
    textDecoration: decoration,
  };

  return (
    <div>
      <div className="video-channel" style={subtitleStytles}>
        <h3>{subHeadingTitle}</h3>
      </div>
    </div>
  );
};

VideoChannel.defaultProps = {
  subHeadingTitle: ": Video Channel Sample",
  textColor: "#fff",
  decoration: "none",
};

VideoChannel.propTypes = {
  subHeadingTitle: PropTypes.string,
  textColor: PropTypes.string,
  decoration: PropTypes.string,
};

export default VideoChannel;
