import { Link } from "react-router-dom";
import { BrowserRouter as Routes } from "react-router-dom";
import Categories from "../../pages/Categories";


function MenuList() {
  return (
    <div>
      <div className="menu-list">
          <ul>
            <Link to="/home">
              <li>Home</li>
            </Link>
            <Link to="/categories">
              <li>Categories</li>
            </Link>
            <Link to="/shorts">
              <li>Shorts</li>
            </Link>
            <Link to="/joblistings">
              <li>Job Listings</li>
            </Link>
            <Link to="/colleges">
              <li>College Connection</li>
            </Link>
            <Link to="/boards">
              <li>Message Boards</li>
            </Link>
            <Link to="/privacy">
              <li>Privacy Policy</li>
            </Link>
          </ul>
      </div>
    </div>
  );
}


export default MenuList;
