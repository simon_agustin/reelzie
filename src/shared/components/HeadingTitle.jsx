import PropTypes from "prop-types";
import React from "react";

function HeadingTitle({headingTitle,textColor,transform,decoration}) {
  const titleStytles = {
    color: textColor,
    textTransform: transform,
    textDecoration: decoration,
  };

  return (
      <div className="heading-title" style={titleStytles}>
        <h1>{headingTitle}</h1>
      </div>
  );
}

HeadingTitle.defaultProps = {
  headingTitle: "reelzie",
  textColor: "#16a8d9",
  transform: "uppercase",
  decoration: "none",
};

HeadingTitle.propTypes = {
  headingitle: PropTypes.string,
  textColor: PropTypes.string,
  transform: PropTypes.string,
  decoration: PropTypes.string,
};

export default HeadingTitle;
