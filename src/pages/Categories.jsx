import CategoryLinks from "../data/CategoryLinks"
import VideoFilter from "../data/VideoFilter"

function Categories() {
  return (
    <div>
      {<CategoryLinks />}
    </div>
  )
}

export default Categories

