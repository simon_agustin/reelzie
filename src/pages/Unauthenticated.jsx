import HeadingTitle from "../shared/components/HeadingTitle";
import { useEffect } from "react";
import CallToAction from "../shared/components/CallToAction";

function Unauthenticated() {
  let options = {
    // root: document.querySelector('#scrollArea'),
    rootMargin: "0px",
    threshold: [0, 0.25, 0.5, 0.75, 1],
  };

  useEffect(() => {
    let sections = document.querySelectorAll(".section-content");

    let observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        // console.log(`${entry.target.className} ${entry.isIntersecting} ${entry.intersectionRatio}`);
        if (entry.intersectionRatio > 0.25) {
          entry.target.style.opacity = 1;
        }
        if (entry.intersectionRatio <= 0.25) {
          entry.target.style.opacity = 0;
        }
      });
    }, options);

    for (let i = 0; i < sections.length; i++) {
      observer.observe(sections[i]);
    }

    return () => {
      //code to be run during unmount phase
      observer.disconnect();
    };
  }, []);

  return (
    <div className="home">
      <div className="heading">
        <div className="section1-background ">
          <div className="section1">
            <div className="section-content">
              {<HeadingTitle />}
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut
                feugiat lorem. Nulla nec pellentesque erat. Mauris maximus enim
                vitae malesuada euismod. Praesent leo purus, suscipit eget
                dignissim id, auctor non odio. Vivamus pellentesque mattis
                lacus. Nulla imperdiet, lacus eget rutrum blandit, lectus magna
                varius est, consequat sollicitudin tortor lectus vitae lorem.
                Integer eget felis erat. Morbi sit amet dapibus justo, nec
                suscipit ligula. Cras cursus eleifend molestie. Pellentesque
                tristique ac nibh non consequat.
              </p>
            </div>
            {/* <CallToAction /> */}
          </div>
        </div>
        <div className="section2-background ">
          <div className="section2">
            <div className="section-content">
              <h1>Learn</h1>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut
                feugiat lorem. Nulla nec pellentesque erat. Mauris maximus enim
                vitae malesuada euismod. Praesent leo purus, suscipit eget
                dignissim id, auctor non odio. Vivamus pellentesque mattis
                lacus. Nulla imperdiet, lacus eget rutrum blandit, lectus magna
                varius est, consequat sollicitudin tortor lectus vitae lorem.
                Integer eget felis erat. Morbi sit amet dapibus justo, nec
                suscipit ligula. Cras cursus eleifend molestie. Pellentesque
                tristique ac nibh non consequat.
              </p>
            </div>
            {/* <CallToAction /> */}
          </div>
        </div>
        <div className="section3-background ">
          <div className="section3">
            <div className="section-content">
              <h1>Get Discovered</h1>
              <p>
                In 1932, Schwab's Pharmacy opened its doors for all of Hollywood
                to shop. It became a popular hangout for the industry's top
                Agents, Managers, and Casting Directors. Hopeful actors, and
                screenwriters would sit at the soda counter at the store waiting
                for those who were shopping to notice them. Once the hotspot
                shut its doors, Hollywood had to find other ways to find the
                next big thing. Until now! Welcome to REELZIE. <br></br>
                <br></br>The time is now, the moment is here. Upload your reels
                or samples of your best work. Hollywood’s doors are opening up
                and now is the perfect time to present the best version of
                yourself to those inside, looking for the future. It only takes
                one person, one discovery to change your life. Give yourself a
                chance!
              </p>
            </div>
            {/* <CallToAction /> */}
          </div>
        </div>
        <div className="section4-background ">
          <div className="section4">
            <div className="section-content">
              <h1>Meet Others</h1>
              <p>
                It’s a big world and Reelzie gives artists the opportunity to
                get acquainted with each other. You never know who you might
                meet or who will be the next big thing, there’s a chance they
                are on Reelzie. So why not take a shot, this industry thrives
                off of the community and connections between each other. Get the
                chance to meet someone new, someone with the same passion and
                goals as you.
              </p>
            </div>
            {/* <CallToAction /> */}
          </div>
        </div>
        <div className="section5-background ">
          <div className="section5">
            <div className="section-content">
              <h1>collaborate</h1>
              <p>
                This industry is all about working with others, sharing ideas to
                be able to create the impossible. Through Reelzie you get the
                opportunity to not only meet other artists but work with them.
                When talent meets talent wonders can come out of it. Your next
                collaborator is waiting. <br></br>
                <br></br>Get Hired: There is always a market for new talent!
                Here is your opportunity to show what you got. People all over
                the world are looking for the next best thing. Who knows it
                might even be you. Talented people come to Reelzie to find
                opportunities to give to all kinds of artists. You have to start
                somewhere and this is just the place. Every avenue matters and
                Reelzie is a home to them all! That is why this is a top
                opportunity for people who are looking to get it. You're just
                one job away.
              </p>
            </div>
            {/* <CallToAction /> */}
          </div>
        </div>
        <div className="section6-background ">
          <div className="section6">
            <div className="section-content">
              <h1>Create</h1>
              <p>
                The time is now! Go out and make your dreams come true. Meet
                incredible people through Reelzie, work with them, get a job and
                never forget to create. Impossible is just an excuse. The
                industry is growing, creating a world we can transport to, this
                is your time! Come and join the fun, use every opportunity. Come
                create the magic that is art, we are providing the tools all you
                need to do is join.
              </p>
            </div>
            {/* <CallToAction /> */}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Unauthenticated;
