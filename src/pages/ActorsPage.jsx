import ActorsTitle from "../shared/components/ActorsTitle"

function ActorsPage() {
  return (
    <div>
      {<ActorsTitle />}
    </div>
  )
}

export default ActorsPage
