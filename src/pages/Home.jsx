import VideoReel from "../shared/components/VideoReel";
import VideoFilter from "../data/VideoFilter";
import VideoChannel from "../shared/components/VideoChannel";
// import React from "react";
// import axios from "axios";

// function test() {
//   const urlProd = 'https://api/reelzie'
//   const urlDev = 'https://dog.ceo/api';
//   const dogRandom = axios.get(`${urlDev}/breeds/image/random`)
//   .then(response => {
//     // const dogs = res.data;
//     // this.setState({ dogRandom });
//     return response.data;
//     // console.log(dogs);
//   })
//   return dogRandom
// }

function Home() {
  // console.log(test())
  // test().
  return (
    <div className="random-reels">
      <VideoFilter />
      <h1>random reels</h1>
      <VideoChannel />
      <div className="video-reel">
        <VideoReel />
        <VideoReel />
        <VideoReel />
        {/* <p>{dogRandom.message}</p> */}
      </div>
    </div>
  );
}

export default Home;
