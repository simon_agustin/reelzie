import SignUpHeader from "../shared/components/SignUp"

function SignUpPage() {
  return (
    <div>
      <SignUpHeader />
    </div>
  )
}

export default SignUpPage
