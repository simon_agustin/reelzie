import "./App.css";
import Header from "./components/Header";
import {BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import CategoryLinks from "./data/CategoryLinks";
import HeadingTitle from "./shared/components/HeadingTitle";
import Home from "./pages/Home";
import Login from "./components/Login";
import Unauthenticated from "./pages/Unauthenticated";
import Categories from "./pages/Categories";
import VideoFilter from "./data/VideoFilter";
import ActorsPage from "./pages/ActorsPage";
import SignUpPage from "./pages/SignUpPage";
import Footer from "./components/Footer";

function App() {

  return (
    <Router>
      <div className="App">
        <Header />
        {/* <NavMenu /> */}
        <div className="opacity">
          <Routes>
            <Route
              path="/unauthenticated"
              element={
                <>
                  {/* <Login /> */}
                  <Unauthenticated />
                </>
              }
            ></Route>
            <Route path="/home" element={<Home />} />
            <Route path="/categories" element={<Categories />} />
            <Route path="/actors" element={<ActorsPage />} />
            <Route path="/signup" element={<SignUpPage />} />
          </Routes>
        </div>
        {/* <CategoryLinks /> */}
        <Footer />
      </div>
    </Router>
  );
}

export default App;
