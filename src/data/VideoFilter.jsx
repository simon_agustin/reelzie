import PropTypes from 'prop-types';
import React from "react";

function VideoFilter({
    videoFilter,
    textColor,
    transform,
}) {

    const linkStyles = {
        color: textColor,
        textTransform: transform,
    }

  return (
    <div className="video-filter">
      <h2 style={linkStyles}>
          {videoFilter}
      </h2>
    </div>
  )
}

VideoFilter.defaultProps = {
    videoFilter: "filter +",
    textColor: "#16a8d9",
    transform: "uppercase"
}

VideoFilter.propTypes = {
    videoFilter: PropTypes.string,
    textColor: PropTypes.string,
    transform: PropTypes.string
}

export default VideoFilter
