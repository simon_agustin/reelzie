import { useState } from "react";
import { Link } from "react-router-dom";

const url = URL;
let dropdown;

function show_hide() {
  if (dropdown === 1) {
    document.getElementsByClassName("sub-link").display = "inline-block";
    return dropdown === 0;
  } else {
    document.getElementsByClassName("sub-link").display = "none";
    return dropdown === 1;
  }; 
}

function CategoryLinks({}) {
  const [CategoryLinks, setCategoryLinks] = useState([
    {
      id: 1,
      text: "Actors",
      url: "/actors",
      children: [
        {
          id: 18,
          text: "TV",
          url: "/test",
        },
        {
          id: 19,
          text: "Film",
          url: "/test",
        },
        {
          id: 20,
          text: "Stage",
          url: "/test",
        },
      ],
    },
    {
      id: 2,
      text: "Directors",
      url: "/directors",
      children: [
        {
          id: 12,
          text: "Residential Editor",
          url: "/test",
        },
        {
          id: 13,
          text: "Independent Editor",
          url: "/test",
        },
      ],
    },
    {
      id: 3,
      text: "Editors",
      url: "/editors",
      children: [
        {
          id: 12,
          text: "Freelance Editor",
          url: "/test",
        },
      ],
    },
    {
      id: 4,
      text: "Writers",
      url: "/writers",
    },
    {
      id: 5,
      text: "Animators",
      url: "/animators",
    },
    {
      id: 6,
      text: "cinematographers",
      url: "/cinematographers",
    },
    {
      id: 7,
      text: "composers",
      url: "/composers",
    },
    {
      id: 8,
      text: "producers",
      url: "/producers",
    },
    {
      id: 9,
      text: "hair",
      url: "/hair",
    },
    {
      id: 10,
      text: "makeup artists",
      url: "/makeup-artists",
    },
    {
      id: 11,
      text: "photographers",
      url: "/photographers",
    },
    {
      id: 12,
      text: "dancers",
      url: "/dancers",
    },
    {
      id: 13,
      text: "custom designers",
      url: "/custom-designers",
    },
    {
      id: 14,
      text: "set designers",
      url: "/set-designers",
    },
    {
      id: 15,
      text: "sound engineers",
      url: "/sound-engineers",
    },
    {
      id: 16,
      text: "musicians",
      url: "/musicians",
    },
    {
      id: 17,
      text: "stunts",
      url: "/stunts",
    },
  ]);


  return (
    <div className="categories">
      <button onClick={show_hide}>click me</button>
      <ul>
        {CategoryLinks.map((link) => (
          <Link to={link.url}>
            <li>{link.text}</li>
            <div className="sub-link">
              <ul>
                {link.children?.map((subLink) => (
                  <Link to={link.url}>
                  <li>{subLink.text}</li>
                  </Link>
                ))}
              </ul>
            </div>
          </Link>
        ))}
        ,
      </ul>
    </div>
  );
}

export default CategoryLinks;
