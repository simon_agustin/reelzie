import PropTypes from "prop-types";

function Footer({
  copyRight,
  privacyPolicy,
  fbIcon,
  igIcon,
  textColor,
  transform,
  decoration,
}) {
  const footerStyles = {
    color: textColor,
    textTransform: transform,
    textDecoration: decoration,
  };
  return (
    <div>
      <footer style={footerStyles}>
        <div className="footer-container">
          <div className="footer-group">
            <div className="copy-right">{copyRight}</div>
            <div className="privacy-policy">{privacyPolicy}</div>
          </div>
        </div>
      </footer>
    </div>
  );
}

Footer.defaultProps = {
  copyRight: "copyright @ 2022",
  privacyPolicy: "privacy policy",
  textColor: "#16a8d9",
  transform: "uppercase",
  decoration: "none",
};

Footer.propTypes = {
  copyRight: PropTypes.string,
  privacyPolicy: PropTypes.string,
  textColor: PropTypes.string,
  transform: PropTypes.string,
  decoration: PropTypes.string,
};

export default Footer;
