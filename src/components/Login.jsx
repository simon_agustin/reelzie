import PropTypes from "prop-types";

function Login ({
        header3,
        bgColor,
        textColor,
        transform,
        decoration,
      }) {
        const headerStyles = {
          backgroundColor: bgColor,
          color: textColor,
          textTransform: transform,
          textDecoration: decoration,
        };
  return (
    <header style={headerStyles}>
    <a href="">
      <h3 className="login">{header3}</h3>
    </a>
    </header>
  );
}

Login.defaultProps = {
    header3: "Login / Sign Up +",
    textColor: "#16a8d9",
    transform: "uppercase",
    decoration: "none",
  };
  
  Login.propTypes = {
    header3: PropTypes.string,
    bgColor: PropTypes.string,
    textColor: PropTypes.string,
    transform: PropTypes.string,
    decoration: PropTypes.string,
  };

export default Login;
