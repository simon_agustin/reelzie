import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleUser } from "@fortawesome/free-solid-svg-icons";
import PropTypes from "prop-types";
import Logo from "../shared/components/Logo";
import MenuBar from "../shared/components/MenuBar";
import NavMenu from "../shared/components/NavMenu";
import MenuList from "../shared/components/MenuList";
import { useState } from "react";
import { useEffect } from "react";
import CallToAction from "../shared/components/CallToAction";

function Header({
  header1,
  header2,
  header3,
  bgColor,
  textColor,
  transform,
  decoration,
}) {
  const headerStyles = {
    backgroundColor: bgColor,
    color: textColor,
    textTransform: transform,
    textDecoration: decoration,
  };

  const [hasNavMenu, setNavMenu] = useState(false);
  useEffect(() => {
    if (window.location.pathname === "/unauthenticated" || window.location.pathname === "/signup") {
      setNavMenu(false);
    } else {
      setNavMenu(true);
    }
  }, []);

  const [hasSignUp, setSignUp] = useState(true);
  useEffect(() => {
    if (window.location.pathname === "/unauthenticated" || window.location.pathname === "/signup" ) {
      setSignUp(true);
    } else {
      setSignUp(false);
    }
  }, []);

  return (
    <header style={headerStyles}>
      <div className="header">
        <Logo header1={header1} />
        <div className="account-bar">
          {hasSignUp ? (
            <div>
              <CallToAction />
            </div>
          ) : (
            ""
          )}
          {hasNavMenu ? (
            <div>
              <NavMenu />
            </div>
          ) : (
            ""
          )}
          {hasNavMenu ? (
            <div className="user-icon">
              <FontAwesomeIcon icon={faCircleUser} size="2x" />
            </div>
          ) : (
            ""
          )}
          {/* <a href=""><h2 className="my-account">{header2}</h2></a> */}
          {hasNavMenu ? <MenuBar /> : ""}
        </div>
      </div>
      <MenuList />
    </header>
  );
}

Header.defaultProps = {
  header1: "reelzie",
  header2: "my account",
  header3: "Login / Sign Up +",
  // bgColor: 'rgb(255,255,255)',
  textColor: "#16a8d9",
  transform: "uppercase",
  decoration: "none",
};

Header.propTypes = {
  header1: PropTypes.string,
  header2: PropTypes.string,
  header3: PropTypes.string,
  bgColor: PropTypes.string,
  textColor: PropTypes.string,
  transform: PropTypes.string,
  decoration: PropTypes.string,
};

// const Text = () => <div>You clicked the button!</div>;

export default Header;
